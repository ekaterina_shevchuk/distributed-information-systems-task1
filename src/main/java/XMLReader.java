import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

public class XMLReader {

    public static Info parseXMLfile(String fileName) {
        Info info = new Info();
        boolean node = false;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
            while (reader.hasNext()) {
                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("node")) {
                        node = false;
                    }
                } else if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("node")) {
                        node = true;
                        Attribute userAttr = startElement.getAttributeByName(new QName("user"));
                        if (userAttr != null) {
                            if (!info.usersEdits.containsKey(userAttr.getValue())) {
                                info.usersEdits.put(userAttr.getValue(), 1);
                            } else {
                                int valueTmp = info.usersEdits.get(userAttr.getValue()) + 1;
                                info.usersEdits.put(userAttr.getValue(), valueTmp);
                            }
                        }
                    } else if (startElement.getName().getLocalPart().equals("tag")) {
                        Attribute keyAttr = startElement.getAttributeByName(new QName("k"));
                        if (keyAttr != null && node) {
                            if (!info.tagsKeys.containsKey(keyAttr.getValue())) {
                                info.tagsKeys.put(keyAttr.getValue(), 1);
                            } else {
                                int valueTmp = info.tagsKeys.get(keyAttr.getValue()) + 1;
                                info.tagsKeys.put(keyAttr.getValue(), valueTmp);
                            }
                        }
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException exc) {
            exc.printStackTrace();
        }
        return info;
    }
}