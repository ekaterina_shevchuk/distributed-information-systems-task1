import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class Main {
    private static final Logger Log = LogManager.getLogger();

    public static void main(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Wrong arguments");
        }
        System.out.println("Hello World!");

        Log.info("File reading start");
        String fileName = "src\\main\\resources\\RU-NVS.osm";
        Info info = XMLReader.parseXMLfile(fileName);

        Log.info("Result writing");
        info.usersEdits.entrySet().stream().sorted((a, b) -> a.getValue() - b.getValue())
            .forEach((a) -> System.out.println("User: " + a.getKey() + " - " + a.getValue()));
        System.out.println("Количество уникальных имен ключей: " + info.tagsKeys.size());
        info.tagsKeys.entrySet().forEach((a) -> System.out.println("Key of tag: " + a.getKey() + " - marked nodes " + a.getValue()));
    }
}
