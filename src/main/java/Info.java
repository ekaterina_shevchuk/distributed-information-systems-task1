import java.util.HashMap;
import java.util.Map;

public class Info {
    public Map<String, Integer> usersEdits;
    public Map<String, Integer> tagsKeys;

    public Info(){
        usersEdits = new HashMap<>();
        tagsKeys = new HashMap<>();
    }

    public void setUsersEdits(Map<String, Integer> usersEdits) {
        this.usersEdits = usersEdits;
    }

    public void setTagsKeys(Map<String, Integer> tagsKeys) {
        this.tagsKeys = tagsKeys;
    }
}
